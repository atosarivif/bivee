# BIVEE Aims
To overcome the crisis, European enterprises need to deeply change introducing continuous improvement and innovation. In order to remain competitive in the globalised economy, an extensive use of knowledge in enterprise management is needed.

Bivee addresses the full lifecycle of the *‘inventive enterprise’* that spans from creativity and idea generation to engineering of new businesses, to continuous production improvement. The approach is aimed at **putting people in the center, with their creativity and competencies, providing a nurturing environment where open thinking and free interaction is more important than formal processes and stringent control** (without relaxing on effective monitoring and performance evaluation).
 
**Objectives**

- Monitoring and assessment of value production processes to identify innovation needs
- Monitoring of world innovation proposals, solutions, etc. to identify innovation opportunities
- Simulation and data analytics to understand pros and cons of innovation options

The project will concretely develop an IT platform and tools to enable enterprises to jointly, rapidly and **virtually catalyse ideas for production system improvements and novel products**, as a response to the inflexibility of the current paradigm. The Bivee platform will be based on a Mission Control Room, for monitoring of Virtual Enterprises **value production** activities, of a Virtual Innovation Factory, for continuous **innovation production** and of an Advanced knowledge repository (PIKR) that includes: technology, models, business domains, competitors, etc. 
At the same time, Bivee will provide means for measuring the success of any process improvement or innovation venture.
